﻿<script src="jquery-1.9.1.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        //Обработка клика по кнопке "Добавить помещение"
        $("#add_newroom").click(function(event) {
            var new_address = $('#new_address').val();
            //var new_name = $('#AddNamePN').val();
            //alert('Нажали на кнопку и передали '+ new_address);
            $.ajax({
                url:"add_newroom.php", //Файл, которому отправляем
                data:{new_address:new_address},
                type:"post", //Метод отправки данных
                cache:false, //Не кешируем 
                success:function(html){
                	window.location.reload();
                   //$("#rooms").load(location.href + " #rooms");//Обновляем список помещений
                }
            })
            return false;
        });
        
    	//Добавление нового ПУ
        $("div.addnewdev").click(function(event) {
            var id_element = $(this).attr('id');
            var id = id_element.split('_')[1];
            //alert(id);
            //var new_name = $('#EditNamePN_'+id).val();
            //alert(new_name);
            $.ajax({
                url:"add_newdev.php",
                data:{id:id},
                type:"post",
                cache:false,
                success:function(html){
                    $("#devices").html(html);
                }
            })
            return false;
        });
        
        //Выборка по расходу ресурсов за текущий год
        $("div.AmountRes").click(function(event) {
            var id_element = $(this).attr('id');
            var id = id_element.split('_')[1];
            //alert(id);
            //var new_name = $('#EditNamePN_'+id).val();
            //alert(new_name);
            $.ajax({
                url:"amount_res.php",
                data:{id:id},
                type:"post",
                cache:false,
                success:function(html){
                    $("#resources").html(html);
                }
            })
            return false;
        });
        
        //Удаление данных
        $("#del_all").click(function(event) {
        	var toDel = confirm("Вы действительно хотите удалить ВСЕ данные?");
        	if (toDel) {var res = 'yes'} else {var res = 'no'};
			//alert(res);
            $.ajax({
                url:"del_all.php",
                data:{res:res},
                type:"post",
                cache:false,
                success:function(html){
                    window.location.reload();
                }
            })
            return false;
        });
        
    });
</script>

<?php include("connect.php"); ?>

<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">
  <title>TestForDev</title>
 </head>
 <body>
  <form method="post">
  <!-- Секция отображения списка помещений -->
  <div id = "rooms">
  	<?php
		echo '
		<h1>Страница сотрудника управляющей организации</h1>
		<h2>Список помещений</h2>
		<div id = "del_all"><input type="submit" name = "submit" value="Очистить списки"></div>
		<table border="1">
		<tr><td>Номер</td><td>Адрес</td><td>Расход</td><td>ПУ</td></tr>
		';
		$q = mysqli_query($con,"Select * from rooms");
		while ($res = mysqli_fetch_assoc($q))
		echo '
			<tr><td>'.$res['id'].'</td><td>'.$res['address'].'</td><td><div id = "AmountRes_'.$res['id'].'" class = "AmountRes"><u>Расход ресурсов</u></div></td><td><div id = "addnewdev_'.$res['id'].'" class = "addnewdev"><u>Добавить ПУ</u></div></td></tr>	
		';
		echo '  
			<tr>
            <td >+</td>
            <td ><input id = "new_address" placeholder="Введите адрес помещения..."></td>
            <td ><div id = "add_newroom"><input type="submit" name = "submit" value="Добавить помещение"></div></td>
             <td ></td>
            </tr>
        </table>
		
		 ';
	?>
  </div>
   	
   	
   	<div id = "devices">
   		
   	</div>
   	
   	<div id = "resources">
   		
   	</div>
   	
  </form>
 </body>
</html>


