<?php
$hostname = "localhost";
$username = "root";
$password = "";
$db_name = "testfordev";
$con = mysqli_connect($hostname, $username, $password, $db_name);

if (!$con)
{
	die("Connection failed: ".mysqli_connect_error());
}
else
{
	mysqli_set_charset($con, "utf8");
}
?>