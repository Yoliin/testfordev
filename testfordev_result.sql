-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Фев 08 2022 г., 12:55
-- Версия сервера: 8.0.15
-- Версия PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `testfordev`
--

DELIMITER $$
--
-- Процедуры
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_newdevice` (IN `number` INT, IN `id_rom` INT, IN `id_res` INT)  NO SQL
    COMMENT 'Добавление ПУ'
Insert into devices (number, id_rom, id_res) VALUES (number, id_rom, id_res)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `add_newroom` (IN `adr` TEXT)  NO SQL
    COMMENT 'Добавить помещение'
insert into rooms (address) VALUES (adr)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `del_all` ()  NO SQL
begin
delete from readings;
delete from devices;
delete from rooms;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `devices`
--

CREATE TABLE `devices` (
  `id` int(11) NOT NULL COMMENT 'Номер',
  `id_rom` int(11) DEFAULT NULL COMMENT 'Номер помещения',
  `id_res` int(11) DEFAULT NULL COMMENT 'Номер типа ресурса',
  `number` int(11) DEFAULT NULL COMMENT 'Номер ПУ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `readings`
--

CREATE TABLE `readings` (
  `id` int(11) NOT NULL COMMENT 'Номер',
  `id_dev` int(11) NOT NULL COMMENT 'Номер ПУ',
  `value` decimal(5,2) NOT NULL COMMENT 'Показание',
  `date_val` date NOT NULL COMMENT 'Дата'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `resources`
--

CREATE TABLE `resources` (
  `id` int(11) NOT NULL COMMENT 'Номер',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Название'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `resources`
--

INSERT INTO `resources` (`id`, `name`) VALUES
(1, 'Э/Э'),
(2, 'ХВС'),
(3, 'ГВС');

-- --------------------------------------------------------

--
-- Структура таблицы `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL COMMENT 'Номер',
  `address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT 'Адрес'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `readings`
--
ALTER TABLE `readings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `resources`
--
ALTER TABLE `resources`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Номер', AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT для таблицы `readings`
--
ALTER TABLE `readings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Номер', AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `resources`
--
ALTER TABLE `resources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Номер', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Номер', AUTO_INCREMENT=46;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
